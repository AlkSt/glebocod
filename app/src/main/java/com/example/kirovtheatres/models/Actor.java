package com.example.kirovtheatres.models;

public class Actor {
    private String _name;
    private String _imageUrl;

    public String GetName(){return _name;}

    public String GetImageUrl(){return _imageUrl;}

    public Actor(String name, String imageUrl){
        _name = name;
        _imageUrl = imageUrl;
    }
}
