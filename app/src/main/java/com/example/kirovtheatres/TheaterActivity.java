package com.example.kirovtheatres;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.kirovtheatres.models.Theater;

public class TheaterActivity extends AppCompatActivity {
    private Theater _theater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theater);

        Bundle intent = getIntent().getExtras();
        _theater = (Theater)intent.getSerializable("theater");

        ImageView theaterImageView = findViewById(R.id.theater_image_view_details);
        theaterImageView.setImageResource(_theater.getImageId());

        TextView nameTextView = findViewById(R.id.theater_name_text_view_details);
        nameTextView.setText(_theater.getName());

        TextView vkTextView = findViewById(R.id.theater_vk_text_view);
        vkTextView.setText(_theater.getVk());

        TextView siteTextView = findViewById(R.id.theater_site_text_view);
        siteTextView.setText(_theater.getSite());

        TextView telephoneTextView = findViewById(R.id.theater_tel_text_view);
        telephoneTextView.setText(_theater.getTelephone());

        TextView addressTextView = findViewById(R.id.theater_address_text_view);
        addressTextView.setText(_theater.getAddress());
    }

    public void TroupeButtonClick(View view) {
        Intent intent = new Intent(getApplicationContext(), ActorsActivity.class);
        intent.putExtra("theater", _theater);
        startActivity(intent);
    }
}
