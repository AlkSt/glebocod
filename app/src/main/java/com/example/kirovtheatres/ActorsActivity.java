package com.example.kirovtheatres;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import com.example.kirovtheatres.adapter.ActorAdapter;
import com.example.kirovtheatres.models.Actor;
import com.example.kirovtheatres.models.Theater;
import com.example.kirovtheatres.services.JSoupActorService;
import com.example.kirovtheatres.services.JSoupService;

import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.ArrayList;

public class ActorsActivity extends AppCompatActivity {
    private TextView _toolbarTextView;
    private RecyclerView _actorRecyclerView;
    private ActorAdapter _actorAdapter;
    private ArrayList<Actor> _actors;
    private Theater _theater;
    protected static ActorsActivity currentActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actors);

        currentActivity = this;

        Bundle intent = getIntent().getExtras();
        _theater = (Theater)intent.getSerializable("theater");

        _toolbarTextView = findViewById(R.id.toolbar_text_view);
        _toolbarTextView.setText(_theater.getName());
        _actorRecyclerView = findViewById(R.id.actors_recycler_view);

        _actors = new ArrayList<Actor>();
        _actors.add(new Actor("Ivan", "https://yandex.ru/collections/card/59ef471f0c1ed200a800ae63/"));
        _actors.add(new Actor("Petya", "https://yandex.ru/collections/card/5e48861c553798d90f0028f2/"));
        _actors.add(new Actor("Ann", "https://yandex.ru/collections/card/58ada8f8b478835f5d186574/"));
        _actors.add(new Actor("Gleb", "https://yandex.ru/collections/card/5dda11be15490610a6dabe82/"));

        _actorRecyclerView.setLayoutManager(new GridLayoutManager(this,2, GridLayoutManager.VERTICAL, false));
        ActorsTask task = new ActorsTask();
        task.execute();
    }

    class ActorsTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                String url = _theater.getTroupeUrl();
                Document document = JSoupService.goToHref(url);


                if (_theater.getName().equals(getResources().getString(R.string.theaterDrama_name))){
                    _actors = JSoupActorService.getDramaTheaterActorsList(document);
                }
                else if (_theater.getName().equals(getResources().getString(R.string.theaterDol_name))){
                    _actors = JSoupActorService.getDollTheaterActorsList(document);
                }
                else if (_theater.getName().equals(getResources().getString(R.string.theaterTUZ_name))){
                    _actors = JSoupActorService.getTUZTheaterActorsList(document);
                }
                else{
                    // wtf
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            _actorAdapter = new ActorAdapter(_actors);
            //_actorRecyclerView.setLayoutManager(new GridLayoutManager(currentActivity,2));
            _actorRecyclerView.setAdapter(_actorAdapter);
        }
    }
}
