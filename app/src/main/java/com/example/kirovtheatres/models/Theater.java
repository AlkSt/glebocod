package com.example.kirovtheatres.models;

import java.io.Serializable;

public class Theater implements Serializable {
    private int _imageId;
    private String _name;
    private String _address;
    private String _site;
    private String _vk;
    private String _telephone;
    private String _troupeUrl;

    public int getImageId(){return _imageId;}
    public void setImageId(int id){_imageId = id;}

    public String getName(){return _name;}
    public String getAddress(){return _address;}
    public String getSite(){return _site;}
    public String getVk(){return _vk;}
    public String getTelephone(){return _telephone;}
    public String getTroupeUrl(){return _troupeUrl;}

    public Theater(String name, String address, String site, String vk, String telephone, String troupeUrl){
        _name = name;
        _address = address;
        _site = site;
        _vk = vk;
        _telephone = telephone;
        _troupeUrl = troupeUrl;
    }
}
