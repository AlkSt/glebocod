package com.example.kirovtheatres;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.kirovtheatres.models.Theater;

public class MainActivity extends AppCompatActivity {
    private String[] _theaterName;
    private String[] _theaterAddress;
    private String[] _theaterVk;
    private String[] _theaterSite;
    private String[] _theaterTelephone;
    private String[] _theaterTroupeUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _theaterName = getResources().getStringArray(R.array.theater_names);
        _theaterAddress = getResources().getStringArray(R.array.theater_address);
        _theaterVk = getResources().getStringArray(R.array.theater_vk);
        _theaterSite = getResources().getStringArray(R.array.theater_site);
        _theaterTelephone = getResources().getStringArray(R.array.theater_telephone);
        _theaterTroupeUrl = getResources().getStringArray(R.array.theater_troupeUrls);
    }


    public void CardClick(View view) throws Exception {
        Intent intent = new Intent(getApplicationContext(), TheaterActivity.class);

        int number;
        switch (view.getId()){
            case R.id.theaterTUZ:
                number = 0;
                break;
            case R.id.theaterDoll:
                number = 1;
                break;
            case R.id.theaterDrama:
                number = 2;
                break;
            default:
                throw new Exception("wtf");
        }

        int id;
        switch (number){
            case 0:
                id = R.drawable.naspasskoy;
                break;
            case 1:
                id = R.drawable.toys;
                break;
            case 2:
                id = R.drawable.drama;
                break;
            default:
                throw new Exception("wtf");
        }

        Theater theater = new Theater(_theaterName[number], _theaterAddress[number], _theaterSite[number],
                _theaterVk[number], _theaterTelephone[number], _theaterTroupeUrl[number]);
        theater.setImageId(id);
        intent.putExtra("theater", theater);

        startActivity(intent);
    }
}
