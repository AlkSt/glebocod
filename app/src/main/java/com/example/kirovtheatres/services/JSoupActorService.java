package com.example.kirovtheatres.services;

import com.example.kirovtheatres.R;
import com.example.kirovtheatres.models.Actor;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class JSoupActorService extends JSoupService {
    public static ArrayList<Actor> getDollTheaterActorsList(Document doc) {
        ArrayList<Actor> act = new ArrayList<>();
        try{
            String s = doc.select("body > table > tbody > tr > td:nth-child(2) > div:nth-child(4) > div:nth-child(6) > span:nth-child(2) > strong > span").first().text();
            Elements images = doc.select("img[src~=(?i)\\.(jpe?g)]");
            int i=0;
            for (Element image : images) {
                if(image.attr("src").contains("assets")) {
                    act.add(new Actor(image.attr("alt"), "kirovdramteatr.ru"+image.attr("src")) );
                }
            }
        }
        catch (Exception ex){

        }
        return act;
        //document.querySelector("body > table > tbody > tr > td:nth-child(2) > div:nth-child(4) > div:nth-child(6) > span:nth-child(2) > strong > span")
    }

    public static ArrayList<Actor> getDramaTheaterActorsList(Document doc) {
        ArrayList<Actor> act = new ArrayList<>();
        try{
            Elements images = doc.select("img[src~=(?i)\\.(jpe?g)]");
            int i=0;
            for (Element image : images) {
                if(image.attr("src").contains("person")) {
                    act.add(new Actor(image.attr("alt"), "https://kirovdramteatr.ru"+image.attr("src")) );
                }
            }
        }
        catch (Exception ex){

        }
        return act;
    }

    public static ArrayList<Actor> getTUZTheaterActorsList(Document doc) {
        ArrayList<Actor> act = new ArrayList<>();
        try{

            String s  ="";
            s =doc.select("div.t_user").first().text().trim();
            String[] ster = s.split("(заслужен.+? артист РСФСР |заслужен.+? артис.+? РФ|артистка, режиссёр|артист, режиссёр|артистка|артист)");
            int i=0;
            Elements images = doc.select("img[src~=(?i)\\.(jpe?g)]");//мб к сыылке спереди https://ekvus-kirov.ru
            for (Element image : images) {
                if(image.attr("src").contains("avatar")) {
                    act.add(new Actor(ster[i++], "https:ekvus-kirov.ru"+image.attr("src")) );

                }

            }
        }
        catch (Exception ex){

        }
        return act;
    }

}
