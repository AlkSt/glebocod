package com.example.kirovtheatres.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.kirovtheatres.R;
import com.example.kirovtheatres.models.Actor;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ActorAdapter extends RecyclerView.Adapter<ActorAdapter.ActorViewHolder> {
    private ArrayList<Actor> _actorList;

    public ActorAdapter(ArrayList<Actor> actors) {
        _actorList = actors;
    }

    @NonNull
    @Override
    public ActorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.actor_table_item, parent, false);
        return new ActorAdapter.ActorViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ActorViewHolder holder, int position) {
        holder.bind(_actorList.get(position));
    }

    @Override
    public int getItemCount() {
        return _actorList.size();
    }

    class ActorViewHolder extends RecyclerView.ViewHolder{
        private ImageView _actorImageView;
        private TextView _actorNameTextView;

        public ActorViewHolder(@NonNull View itemView) {
            super(itemView);
            _actorImageView = itemView.findViewById(R.id.table_actor_image_view);
            _actorNameTextView = itemView.findViewById(R.id.table_actor_name_text_view);
        }

        public void bind(Actor actor) {
            _actorNameTextView.setText(actor.GetName());
            String actorPhotoUrl = actor.GetImageUrl();
            Picasso.with(itemView.getContext())
                    .load(actorPhotoUrl)
                    .resize(500, 500)
                    .centerInside()
                    .into(_actorImageView);
            _actorImageView.setVisibility(actorPhotoUrl != null ? View.VISIBLE : View.GONE);
        }

    }
}
