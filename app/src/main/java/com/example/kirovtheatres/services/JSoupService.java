package com.example.kirovtheatres.services;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class JSoupService {
    public static void ignoreSsl(){
        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {

            @Override
            public X509Certificate[] getAcceptedIssuers() {
// TODO Auto-generated method stub
                return null;
            }

            @Override
            public void checkServerTrusted(X509Certificate[] arg0, String arg1){
// TODO Auto-generated method stub

            }

            @Override
            public void checkClientTrusted(X509Certificate[] arg0, String arg1){
// TODO Auto-generated method stub

            }
        }};

// Install the all-trusting trust manager
        try {
            SSLContext sc = SSLContext.getInstance("TLS");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        } catch (Exception e) {
            ;
        }
    }

    public static Document goToHref(String url) throws IOException {
        ignoreSsl();
        return Jsoup.connect(url).get();
    }
//    public static Document goToHref(String url) throws IOException {
//
////        return  Jsoup.connect(url).get();
////        Document doc = Jsoup.connect(String.format("https://www.gismeteo.ru/weather-%s/%s"))
////                .userAgent("Chrome/4.0.249.0 Safari/532.5")
////                .referrer("http://www.google.com")
////                .get();
//        Document doc = Jsoup.connect(url).userAgent("Mozilla/5.0(Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0")
//                .get();
//        String a = doc.text();
//        return doc;
//
//    }
}
